package com.tsc.skuschenko.tm.repository.dto;

import com.tsc.skuschenko.tm.dto.AbstractEntityDTO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AbstractDTORepository<E extends AbstractEntityDTO>
        extends JpaRepository<E, String> {

}
