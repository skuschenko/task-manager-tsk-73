package com.tsc.skuschenko.tm.component;

import com.tsc.skuschenko.tm.api.service.IBroadcastService;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

@Getter
@Component
public class JmsMessageComponent {

    @NotNull
    public static JmsMessageComponent instance;

    @NotNull
    @Autowired
    private IBroadcastService broadcastService;

    @NotNull
    private Connection connection;

    @NotNull
    private ConnectionFactory connectionFactory;

    public JmsMessageComponent() {
        init();
        instance = this;
    }

    @NotNull
    public static JmsMessageComponent getInstance() {
        return instance;
    }

    @SneakyThrows
    private void init() {
        connectionFactory = new ActiveMQConnectionFactory(
                ActiveMQConnectionFactory.DEFAULT_BROKER_URL
        );
        connection = connectionFactory.createConnection();
    }

    public void run() throws JMSException {
        connection.start();
    }

    public void shutdown() throws JMSException {
        connection.close();
        broadcastService.shutdown();
    }

}
