package com.tsc.skuschenko.tm.repository.dto;

import com.tsc.skuschenko.tm.dto.SessionDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SessionDTORepository
        extends AbstractDTORepository<SessionDTO> {

    @Query("SELECT e FROM SessionDTO e WHERE e.userId = :userId")
    @Nullable List<SessionDTO> findAll(@Param("userId") @NotNull String userId);

    @Query("SELECT e FROM SessionDTO e WHERE e.id = :id")
    @Nullable SessionDTO findSessionById(@Param("id") @NotNull String id);

    @Query("DELETE FROM SessionDTO e WHERE e.id = :id")
    void removeSessionById(@Param("id") @NotNull String id);

}
