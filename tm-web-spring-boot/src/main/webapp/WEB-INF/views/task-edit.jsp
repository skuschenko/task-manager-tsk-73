<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Project edit</h1>

<form:form action="/task/edit/${task.id}/" method="post" modelAttribute="task">
	<div class = "row row_1">
		<div class = "col col_1">
			<div class = "field__title">Name:</div>
			<div class = "field__value">
				<form:input type="text" path="name"/>
			</div>
		</div>
	</div>
	<div class = "row row_2">
		<div class = "col col_1">
			<div class = "field__title">Status:</div>
			<div class = "field__value">
				<form:select path="status">
					<form:option value="${null}" label = ""/>
					<form:options items="${statuses}" itemLabel = "displayName"/>
				</form:select>
			</div>
		</div>
	</div>
	<div class = "row row_3">
		<div class = "col col_1">
			<div class = "field__title">Description:</div>
			<div class = "field__value">
				<form:input type="text" path="description"/>
			</div>
		</div>
	</div>
	<div class = "row row_4">
		<div class = "col col_1">
			<div class = "field__title">Start Date:</div>
			<div class = "field__value">
				<form:input type="date" path="dateStart"/>
			</div>
		</div>
	</div>
	<div class = "row row_5">
		<div class = "col col_1">
			<div class = "field__title">End Date:</div>
			<div class = "field__value">
				<form:input type="date" path="dateFinish"/>
			</div>
		</div>
	</div>
		<div class = "row row_6">
    		<div class = "col col_1">
    			<div class = "field__title">Project:</div>
    			<div class = "field__value">
    				<form:select path="projectId">
    					<form:option value="${null}" label = ""/>
    					<form:options items="${projects}" itemLabel = "name" itemValue="id"/>
    				</form:select>
    			</div>
    		</div>
    	</div>
	<div class = "row row_7">
		<div class = "col col_1">
			<button type="submit">Save task</button>
		</div>
	</div>
	<form:input type="hidden" path="id" />
</form:form>

<jsp:include page="../include/_footer.jsp"/>