package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.endpoint.ITaskRestEndpoint;
import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
@WebService(
        endpointInterface = "com.tsc.skuschenko.tm.api.endpoint." +
                "ITaskRestEndpoint"
)
public class TaskEndpoint implements ITaskRestEndpoint {

    @NotNull
    static final String CREATE_ALL_METHOD = "/createAll";

    @NotNull
    static final String CREATE_METHOD = "/create";

    @NotNull
    static final String DELETE_ALL_METHOD = "/deleteAll";

    @NotNull
    static final String DELETE_BY_ID_METHOD = "/deleteById/{id}";

    @NotNull
    static final String FIND_ALL_METHOD = "/findAll";

    @NotNull
    static final String FIND_BY_ID_METHOD = "/findById/{id}";

    @NotNull
    static final String SAVE_ALL_METHOD = "/saveAll";

    @NotNull
    static final String SAVE_METHOD = "/save";

    @Autowired
    @NotNull
    private ITaskService taskService;

    @Override
    @WebMethod
    @PostMapping(CREATE_METHOD)
    public void create(
            @WebParam(name = "task")
            @RequestBody @NotNull final Task task
    ) {
        taskService.save(task);
    }

    @Override
    @WebMethod
    @PostMapping(CREATE_ALL_METHOD)
    public void createAll(
            @WebParam(name = "tasks")
            @RequestBody @NotNull final Collection<Task> tasks
    ) {
        tasks.forEach(taskService::save);
    }

    @Override
    @WebMethod
    @DeleteMapping(DELETE_ALL_METHOD)
    public void deleteAll(
            @WebParam(name = "tasks")
            @RequestBody @NotNull final Collection<Task> tasks
    ) {
        tasks.forEach(item -> taskService.removeById(item.getId()));
    }

    @Override
    @WebMethod
    @DeleteMapping(DELETE_BY_ID_METHOD)
    public void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        taskService.removeById(id);
    }

    @Override
    @WebMethod
    @GetMapping(FIND_BY_ID_METHOD)
    public Task find(
            @WebParam(name = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping(FIND_ALL_METHOD)
    public Collection<Task> findAll() {
        return taskService.findAll();
    }

    @Override
    @WebMethod
    @PutMapping(SAVE_METHOD)
    public void save(
            @WebParam(name = "task")
            @RequestBody @NotNull final Task task
    ) {
        taskService.save(task);
    }

    @Override
    @WebMethod
    @PutMapping(SAVE_ALL_METHOD)
    public void saveAll(
            @WebParam(name = "tasks")
            @RequestBody @NotNull final Collection<Task> tasks
    ) {
        tasks.forEach(taskService::save);
    }

}
