package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IProjectService {

    void clearAll();

    @NotNull Project create(@Nullable String name);

    @Nullable Collection<Project> findAll();

    @Nullable Project findById(@Nullable String id);

    void remove(@Nullable Project project);

    void removeById(@Nullable String id);

    void save(@NotNull Project project);

}
