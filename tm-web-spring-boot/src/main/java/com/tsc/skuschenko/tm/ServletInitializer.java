package com.tsc.skuschenko.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer
        extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(
            @NotNull final SpringApplicationBuilder builder
    ) {
        return builder.sources(Application.class);
    }

}