package com.tsc.skuschenko.tm.controller;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.configuration.DataBaseConfiguration;
import com.tsc.skuschenko.tm.configuration.WebApplicationConfiguration;
import com.tsc.skuschenko.tm.marker.UnitWebCategory;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.UserUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collection;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {
                WebApplicationConfiguration.class,
                DataBaseConfiguration.class
        }
)
public class TaskControllerTest {

    @NotNull
    private static final String CREATE = "/task/create";

    @NotNull
    private static final String DELETE = "/task/delete/";

    @NotNull
    private static final String TASKS = "/tasks";

    @Autowired
    @NotNull
    ITaskService taskService;

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void create() {
        this.mockMvc.perform(MockMvcRequestBuilders.get(CREATE))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @NotNull final Collection<Task> tasks =
                taskService.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void delete() {
        @NotNull final Task task = new Task("pro");
        task.setUserId(UserUtil.getUserId());
        taskService.save(task);
        Assert.assertNotNull(taskService.findById(task.getId()));
        this.mockMvc.perform(
                MockMvcRequestBuilders.get(DELETE + task.getId())
        ).andDo(print())
                .andExpect(status().is3xxRedirection());
        @NotNull final Task taskNew =
                taskService.findById(task.getId());
        Assert.assertNull(taskNew);
    }

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext()
                .setAuthentication(authentication);
        taskService.clearAll();
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void taskList() {
        @NotNull final Task task = new Task("pro");
        task.setUserId(UserUtil.getUserId());
        taskService.save(task);
        this.mockMvc.perform(MockMvcRequestBuilders.get(TASKS))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
