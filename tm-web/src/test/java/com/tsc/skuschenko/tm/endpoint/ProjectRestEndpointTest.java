package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.marker.IntegrationCategory;
import com.tsc.skuschenko.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class ProjectRestEndpointTest {

    @Test
    @Category(IntegrationCategory.class)
    public void testChangeStatusById() {

    }

    @NotNull
    static final String API_URL = "http://localhost:8080/api/projects/";

    @NotNull
    static final String CREATE_METHOD = "create";

    @NotNull
    static final String CREATE_ALL_METHOD = "createAll";

    @NotNull
    static final String DELETE_BY_ID_METHOD = "deleteById/";

    @NotNull
    static final String DELETE_ALL_METHOD = "deleteAll";

    @NotNull
    static final String FIND_BY_ID_METHOD = "findById/";

    @NotNull
    static final String FIND_ALL_METHOD = "findAll";

    @NotNull
    static final String SAVE_METHOD = "save";

    @NotNull
    static final String SAVE_ALL_METHOD = "saveAll";

    @Before
    public void before() {
        deleteAll();
        create();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void create() {
        @NotNull String url = API_URL + CREATE_METHOD;
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final Project project = new Project("pro");
        @NotNull final HttpEntity<Project> requestUpdate =
                new HttpEntity<>(project, headers);
        restTemplate.postForObject(url, requestUpdate, Project.class);
        url = API_URL + FIND_BY_ID_METHOD + project.getId();
        @NotNull final ResponseEntity<Project> response
                = restTemplate.getForEntity(url, Project.class);
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final Project projectNew = response.getBody();
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(project.getId(), projectNew.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void createAll() {
        @NotNull final String url = API_URL + CREATE_ALL_METHOD;
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_ATOM_XML);
        @NotNull final List<Project> projects = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            projects.add(new Project("pro" + i));
        }
        @NotNull final HttpEntity<List> requestUpdate =
                new HttpEntity<>(projects, headers);
        restTemplate.postForObject(url, requestUpdate, List.class);
        @Nullable final List<Project> projectsNew = findAllProjects();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteById() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final Project project = findAllProjects().get(0);
        @NotNull final String url =
                API_URL + DELETE_BY_ID_METHOD + project.getId();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<Project> requestUpdate =
                new HttpEntity<>(project, headers);
        restTemplate.exchange(
                url, HttpMethod.DELETE, requestUpdate, Project.class
        );
        Assert.assertEquals(0, findAllProjects().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final List<Project> projects = findAllProjects();
        @NotNull final String url =
                API_URL + DELETE_ALL_METHOD;
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<Project[]> requestUpdate =
                new HttpEntity<>(projects.toArray(new Project[0]), headers);
        restTemplate.exchange(
                url, HttpMethod.DELETE, requestUpdate, Project[].class
        );
        Assert.assertEquals(0, findAllProjects().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        @NotNull final List<Project> projects = findAllProjects();
        Assert.assertNotNull(projects);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + FIND_BY_ID_METHOD +
                projects.get(0).getId();
        @NotNull final ResponseEntity<Project> response
                = restTemplate.getForEntity(url, Project.class);
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        findAllProjects();
    }

    @NotNull
    private List<Project> findAllProjects() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + FIND_ALL_METHOD;
        @NotNull final ResponseEntity<List<Project>> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Project>>() {
                }
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        Assert.assertNotNull(response.getBody());
        return response.getBody();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + SAVE_METHOD;
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final Project project = findAllProjects().get(0);
        project.setDescription(UUID.randomUUID().toString());
        @NotNull final HttpEntity<Project> requestUpdate =
                new HttpEntity<>(project, headers);
        restTemplate.exchange(
                url, HttpMethod.PUT, requestUpdate, Project.class
        );
        Assert.assertEquals(
                project.getDescription(),
                findAllProjects().get(0).getDescription()
        );
    }

    @Test
    @Category(IntegrationCategory.class)
    public void saveAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = API_URL + SAVE_ALL_METHOD;
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final List<Project> projects = findAllProjects();
        projects.forEach(item ->
                item.setDescription(UUID.randomUUID().toString())
        );
        @NotNull final HttpEntity<List<Project>> requestUpdate =
                new HttpEntity<>(projects, headers);
        restTemplate.exchange(
                url, HttpMethod.PUT, requestUpdate, Project.class
        );
        @Nullable final List<Project> projectsNew = findAllProjects();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

}
