package com.tsc.skuschenko.tm.integration.soap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.skuschenko.tm.api.endpoint.IAuthSoapEndpoint;
import com.tsc.skuschenko.tm.api.endpoint.IUserRestEndpoint;
import com.tsc.skuschenko.tm.client.soap.AuthSoapClient;
import com.tsc.skuschenko.tm.client.soap.UserSoapClient;
import com.tsc.skuschenko.tm.marker.IntegrationWebCategory;
import com.tsc.skuschenko.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.*;

public class UserRestEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";

    @Nullable
    private static String USER_ID = null;

    @NotNull
    private static IAuthSoapEndpoint authEndpoint;

    @NotNull
    private static IUserRestEndpoint endpoint;

    @AfterClass
    public static void afterClass() {
        logout();
    }

    public static void auth() throws MalformedURLException {
        authEndpoint = AuthSoapClient.getInstance(URL);
        Assert.assertTrue(
                authEndpoint.login("test", "test").getSuccess()
        );
        endpoint = UserSoapClient.getInstance(URL);
        setSession();
    }

    @SneakyThrows
    @BeforeClass
    public static void beforeClass() {
        auth();
    }

    public static void logout() {
        Assert.assertTrue(authEndpoint.logout().getSuccess());
    }

    private static void setSession() throws MalformedURLException {
        @NotNull final BindingProvider bindingProvider =
                (BindingProvider) authEndpoint;
        @NotNull final ObjectMapper oMapper = new ObjectMapper();
        @NotNull final Object headers = bindingProvider.getResponseContext()
                .get(MessageContext.HTTP_RESPONSE_HEADERS);
        @NotNull final Map<String, String> mapHeaders =
                oMapper.convertValue(headers, Map.class);
        @NotNull final Object cookieValue =
                mapHeaders.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> collection = (List<String>) cookieValue;
        ((BindingProvider) endpoint).getRequestContext().put(
                MessageContext.HTTP_REQUEST_HEADERS,
                Collections.singletonMap(
                        "Cookie",
                        Collections.singletonList(collection.get(0))
                )
        );
    }

    @Before
    public void before() {
        create();
        deleteByLogin();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void create() {
        @NotNull final User user = new User();
        user.setLogin("test1");
        user.setPasswordHash("test1");
        endpoint.create(user);
        @Nullable final User userNew = endpoint.find(user.getId());
        Assert.assertNotNull(userNew);
        Assert.assertEquals(user.getId(), userNew.getId());
        USER_ID = user.getId();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void deleteByLogin() {
        endpoint.deleteByLogin("test1");
        Assert.assertNull(findById());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findAll() {
        Assert.assertNotEquals(0, findAllUsers().size());
    }

    @NotNull
    private List<User> findAllUsers() {
        @Nullable final Collection<User> users = endpoint.findAll();
        if (!Optional.ofNullable(users).isPresent())
            return new ArrayList<>();
        return new ArrayList<>(users);
    }

    @Nullable
    public User findById() {
        @Nullable final User user = endpoint.find(USER_ID);
        Assert.assertNotNull(user);
        return user;
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void findByIdTest() {
        findById();
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void save() {
        @Nullable final User user = findById();
        Assert.assertNotNull(user);
        user.setLastName(UUID.randomUUID().toString());
        endpoint.save(user);
        Assert.assertEquals(user.getLastName(), findById().getLastName());
    }

}
