package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.configuration.DataBaseConfiguration;
import com.tsc.skuschenko.tm.enumerated.RoleType;
import com.tsc.skuschenko.tm.marker.UnitWebCategory;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
public class UserServiceTest {

    @Autowired
    IUserService userService;

    @After
    public void after() {
        userService.clear();
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testCreate() {
        @NotNull final User user = userService.create("user1", "user1");
        Assert.assertNotNull(user);
        Assert.assertEquals("user1", user.getLogin());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testCreateWithEmail() {
        @NotNull final User user =
                userService.create("user1", "user1", "user1@user1.ru");
        Assert.assertNotNull(user);
        Assert.assertEquals("user1", user.getLogin());
        Assert.assertEquals("user1@user1.ru", user.getEmail());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testCreateWithRole() {
        @NotNull final User user =
                userService.create("user1", "user1", RoleType.USER);
        Assert.assertNotNull(user);
        Assert.assertEquals("user1", user.getLogin());
        Assert.assertEquals(1, user.getRoles().size());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testFindByEmail() {
        userService.create("user1", "user1", "user1@user1.ru");
        @Nullable final User userFind =
                userService.findByEmail("user1@user1.ru");
        Assert.assertNotNull(userFind);
        Assert.assertEquals("user1@user1.ru", userFind.getEmail());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testFindByLogin() {
        userService.create("user1", "user1");
        @Nullable final User userFind =
                userService.findByLogin("user1");
        Assert.assertNotNull(userFind);
        Assert.assertEquals("user1", userFind.getLogin());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testLockUserByLogin() {
        @Nullable final User user = userService.create("user1", "user1");
        Object roles = userService.findAllRoles();
        user.setLocked(true);
        userService.lockUserByLogin(user.getLogin());
        @Nullable final User userFind =
                userService.findByLogin("user1");
        Assert.assertNotNull(userFind);
        Assert.assertTrue(userFind.isLocked());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testRemoveByLogin() {
        @Nullable final User user = userService.create("user1", "user1");
        userService.removeByLogin(user.getLogin());
        @Nullable final User userFind = userService.findByLogin("user1");
        Assert.assertNull(userFind);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testUnlockUserByLogin() {
        @Nullable final User user = userService.create("user1", "user1");
        user.setLocked(false);
        @Nullable final User userFind =
                userService.findByLogin("user1");
        Assert.assertNotNull(userFind);
        Assert.assertFalse(userFind.isLocked());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testUpdateUser() {
        @Nullable final User user = userService.create("user1", "user1");
        user.setFirstName("FirstName");
        user.setMiddleName("MiddleName");
        user.setLastName("LastName");
        Assert.assertNotNull(user.getFirstName());
        Assert.assertNotNull(user.getLastName());
        Assert.assertNotNull(user.getMiddleName());
        userService.save(user);
        @Nullable final User userFind =
                userService.findByLogin("user1");
        Assert.assertEquals("FirstName", userFind.getFirstName());
        Assert.assertEquals("MiddleName", userFind.getMiddleName());
        Assert.assertEquals("LastName", userFind.getLastName());
    }

}
