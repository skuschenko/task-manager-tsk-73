package com.tsc.skuschenko.tm.api.endpoint;

import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/users")
public interface IUserRestEndpoint {

    @WebMethod
    @PostMapping("/create")
    void create(@RequestBody @NotNull final User user);

    @WebMethod
    @DeleteMapping("/deleteByLogin/{login}")
    void deleteByLogin(@PathVariable("login") @NotNull String login);

    @WebMethod
    @GetMapping("/findById/{id}")
    User find(@PathVariable("id") @NotNull String id);

    @WebMethod
    @GetMapping("/findAll")
    Collection<User> findAll();

    @WebMethod
    @PutMapping("/save")
    void save(@RequestBody @NotNull User user);

}
