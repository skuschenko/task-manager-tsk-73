package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.endpoint.IAuthSoapEndpoint;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.exception.entity.user.UserNotFoundException;
import com.tsc.skuschenko.tm.model.Result;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Optional;

@Component
@WebService(
        endpointInterface = "com.tsc.skuschenko.tm.api.endpoint." +
                "IAuthSoapEndpoint"
)
public class AuthEndpointSOAP implements IAuthSoapEndpoint {

    @Resource
    @NotNull
    private AuthenticationManager authenticationManager;

    @Autowired
    @NotNull
    private IUserService userService;

    @WebMethod
    public Result login(
            @WebParam(name = "username") @NotNull final String username,
            @WebParam(name = "password") @NotNull final String password
    ) {
        try {
            @Nullable final User user = userService.findByLogin(username);
            Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
            @NotNull final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(
                            username,
                            password
                    );
            @NotNull final Authentication authentication =
                    authenticationManager.authenticate(token);
            SecurityContextHolder
                    .getContext()
                    .setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (@NotNull final Exception e) {
            return new Result(e);
        }
    }

    @WebMethod
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

    @WebMethod
    public User profile() {
        @NotNull final SecurityContext securityContext =
                SecurityContextHolder.getContext();
        @NotNull final Authentication authentication =
                securityContext.getAuthentication();
        return userService.findByLogin(authentication.getName());
    }

}
