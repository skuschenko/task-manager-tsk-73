package com.tsc.skuschenko.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

    @GetMapping("/")
    @NotNull
    public ModelAndView index() {
        return new ModelAndView("index");
    }

}
