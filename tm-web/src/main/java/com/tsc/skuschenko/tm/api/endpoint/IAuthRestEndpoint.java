package com.tsc.skuschenko.tm.api.endpoint;

import com.tsc.skuschenko.tm.model.Result;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/api/auth")
public interface IAuthRestEndpoint {

    @GetMapping("/login")
    Result login(
            @RequestParam("username") @NotNull final String username,
            @RequestParam("password") @NotNull final String password
    );

    @GetMapping("/logout")
    Result logout();

    @GetMapping(value = "/profile", produces = "application/json")
    User profile();

    @GetMapping(value = "/session", produces = "application/json")
    Authentication session();

}
