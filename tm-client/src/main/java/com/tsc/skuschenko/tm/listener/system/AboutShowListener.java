package com.tsc.skuschenko.tm.listener.system;

import com.jcabi.manifests.Manifests;
import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.event.ConsoleEvent;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class AboutShowListener extends AbstractListener {

    private static final String ARGUMENT = "-a";

    private static final String DESCRIPTION = "about";

    private static final String NAME = "about";

    @Autowired
    private ISessionService sessionService;

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@aboutShowListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        System.out.println("AUTHOR: " + Manifests.read("developer"));
        System.out.println("EMAIL: " + Manifests.read("email"));
    }

    @Override
    public String name() {
        return NAME;
    }

}
