package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Session;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

@Service
@Getter
@Setter
public class SessionService implements ISessionService {

    @Nullable
    private Session session;

}
